import React, { useEffect, useState } from 'react'
import Helmet from 'react-helmet'

import ClashRuler from '../components/ClashRuler'
import ImagePicker from '../components/ImagePicker'

const App = () => {
  const [base64Image, setBase64Image] = useState(null)
  const [image, setImage] = useState(null)
  useEffect(() => {
    if (base64Image !== null) {
      const imageElement = new Image()
      imageElement.onload = () => {
        setImage(imageElement)
      }
      imageElement.src = base64Image
    }
  }, [base64Image])

  return (
    <>
      <Helmet>
        <title>Clash Ruler</title>
      </Helmet>
      {
        image === null
        ? <ImagePicker setImage={setBase64Image}/>
        : <ClashRuler image={image}/>
      }
    </>
  )
}

export default App
