import { useState } from 'react'
import { complex } from 'mathjs'

export const PRESETS = {
  'LIGHT': { name: 'Light', radius: 2, color: '#006ffe' },
  'EARTHQUAKE5': { name: 'Earthquake 5', radius: 4.7, color: '#8c4c0c' },
  'EARTHQUAKE4': { name: 'Earthquake 4', radius: 4.4, color: '#8c4c0c' },
  'EARTHQUAKE3': { name: 'Earthquake 3', radius: 4.1, color: '#8c4c0c' },
  'EARTHQUAKE2': { name: 'Earthquake 2', radius: 3.8, color: '#8c4c0c' },
  'EARTHQUAKE1': { name: 'Earthquake 1', radius: 3.5, color: '#b88450' },
  'CUSTOM': { name: 'Custom', radius: 1, color: '#000000' },
}

const CUSTOM_PRESET = 'CUSTOM'
const DEFAULT_PRESET = 'LIGHT'

const useSpell = () => {
  const [preset, _setPreset] = useState(null)
  const [radius, _setRadius] = useState(null)
  const [color, _setColor] = useState(null)
  const [center, setCenter] = useState(complex(0, 0))

  const setPreset = (preset) => {
    const spell = PRESETS[preset]
    _setPreset(preset)
    _setRadius(spell.radius)
    _setColor(spell.color)
  }
  const setRadius = (radius) => {
    _setPreset(CUSTOM_PRESET)
    _setRadius(radius)
  }
  const setColor = (color) => {
    _setPreset(CUSTOM_PRESET)
    _setColor(color)
  }

  if (preset === null) {
    setPreset(DEFAULT_PRESET)
  }

  return {
    center,
    setCenter,
    radius,
    setRadius,
    color,
    setColor,
    preset,
    setPreset,
  }
}

export default useSpell
