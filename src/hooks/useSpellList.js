import { useState } from 'react'
import { v4 as uuidv4 } from 'uuid'

const useSpellList = () => {
  const [spells, setSpells] = useState([])

  const addSpell = (spell) => {
    spell = {
      ...spell,
      key: uuidv4(),
    }
    setSpells([...spells, spell])
  }
  const removeSpell = (key) => {
    setSpells(spells.filter(spell => spell.key !== key))
  }

  return {
    spells,
    addSpell,
    removeSpell,
  }
}

export default useSpellList
