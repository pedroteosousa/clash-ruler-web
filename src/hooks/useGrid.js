import { useState } from 'react'
import { add, complex, inv, subset, subtract, index, matrix, multiply } from 'mathjs'

export const AXIS1 = complex(0.8, 0.6)
export const AXIS2 = complex(0.8, -0.6)

export const STAGES = {
  ORIGIN: 'origin',
  TILE_SIZE: 'tilesize',
  DONE: 'done',
}

const useGrid = (initialOrigin) => {
  const [stage, _setStage] = useState(STAGES.ORIGIN)
  const [origin, _setOrigin] = useState(initialOrigin)
  const [tileSize, _setTileSize] = useState(0)
  const [transform, _setTransform] = useState(null)

  const updateOrigin = (_origin) => {
    _setOrigin(_origin)
  }
  const setOrigin = (_origin) => {
    updateOrigin(_origin)
    _setStage(STAGES.TILE_SIZE)
  }
  const updateTileSize = (_tileSize) => {
    _setTileSize(_tileSize)
  }
  const setTileSize = (_tileSize) => {
    updateTileSize(_tileSize)
    _setTransform(multiply(matrix([[AXIS1.re, AXIS2.re], [AXIS1.im, AXIS2.im]]), _tileSize))
    _setStage(STAGES.DONE)
  }
  const toPixels = (point) => {
    const diff = multiply(transform, matrix([point.re, point.im]))
    return add(complex(subset(diff, index(0)), subset(diff, index(1))), origin)
  }
  const toPoint = (pixels) => {
    const diff = subtract(pixels, origin)
    const mat = multiply(inv(transform), matrix([diff.re, diff.im]))
    return complex(subset(mat, index(0)), subset(mat, index(1)))
  }

  return {
    stage,
    origin,
    updateOrigin,
    setOrigin,
    tileSize,
    updateTileSize,
    setTileSize,
    transform,
    toPixels,
    toPoint,
  }
}

export default useGrid
