import React from 'react'
import { Line } from 'react-konva'
import { complex, subtract } from 'mathjs'

export const tileEdges = (edge) => {
  const x = edge.re, y = edge.im
  return [complex(x, y), complex(x + 1, y), complex(x + 1, y + 1), complex(x, y + 1)]
}

const Tile = ({edge, grid}) => {
  const edges = tileEdges(edge)
  const points = []
  for (let i in edges) {
    const point = subtract(edges[i], edges[0])
    const pixel = subtract(grid.toPixels(point), grid.origin)
    points.push(pixel.re)
    points.push(pixel.im)
  }
  const center = grid.toPixels(edges[0])
  return (
    <Line
      x={center.re}
      y={center.im}
      points={points}
      stroke="red"
      strokeWidth={2}
      closed
    />
  )
}

export default Tile
