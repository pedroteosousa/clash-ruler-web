import React from 'react'

import SpellCard from './SpellCard'
import SidebarSectionTitle from './SidebarSectionTitle'

export const SpellList = ({spells, onHoverStart, onHoverEnd, deleteSpell}) => (
  <div>
    <SidebarSectionTitle>
      Spell List
    </SidebarSectionTitle>
    {
      spells.map((spell, index) => (
        <SpellCard
          spell={spell}
          deleteSpell={deleteSpell}
          onEnter={onHoverStart}
          onLeave={onHoverEnd}
          index={index}
          key={spell.key}
        />
      ))
    }
  </div>
)

export default SpellList
