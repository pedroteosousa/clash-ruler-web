import React from 'react'
import styled from 'styled-components'

import CloseIcon from './icons/CloseIcon'

const Container = styled.div`
  padding: 5px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 20px;
  border-left: 6px solid ${props => props.borderColor || 'black'};
  background-color: ${props => props.index % 2 ? '#ffffff' : '#f8f8f8' };
`

const CloseButtonContainer = styled.div`
  display: flex;
  align-items: center;
`

export const SpellCard = ({spell, onEnter, onLeave, deleteSpell, index}) => (
  <Container
    onMouseMove={() => {onEnter(spell.key)}}
    onMouseEnter={() => {onEnter(spell.key)}}
    onMouseLeave={() => {onLeave(spell.key)}}
    borderColor={spell.color}
    index={index}
  >
    <p>Spell #{index + 1}</p>
    <CloseButtonContainer
      onClick={() => deleteSpell(spell.key)}
    >
      <CloseIcon/>
    </CloseButtonContainer>
  </Container>
)

export default SpellCard
