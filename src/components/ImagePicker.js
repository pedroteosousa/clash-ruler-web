import React, { useEffect } from 'react'
import styled from 'styled-components'
import { useFilePicker, utils } from 'react-sage'

const Container = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`
const Button = styled.div`
  padding: 20px;
  border: 2px dashed black;
  display: flex;
  align-items: center;
  justify-content: center;
`

const ImagePicker = ({setImage}) => {
  const { files, onClick, HiddenFileInput } = useFilePicker()

  useEffect(() => {
    const getDataUrls = async () => {
      const data = await Promise.all(files.map(utils.loadFile))
      setImage(data)
    }
    if (files.length) {
      getDataUrls()
    }
  }, [files, setImage])

  return (
    <Container>
      <Button onClick={onClick}>UPLOAD IMAGE</Button>
      <HiddenFileInput accept=".jpg, .jpeg, .png" multiple={false} />
    </Container>
  )
}

export default ImagePicker
