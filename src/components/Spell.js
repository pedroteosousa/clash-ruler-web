import React, { Component } from 'react'
import { Circle, Line } from 'react-konva'
import { Complex, abs, complex, norm, subtract } from 'mathjs'

import Tile, { tileEdges } from './Tile'

const SPELL_RESOLUTION = 100

class Spell extends Component {
  intersects(tile) {
    const inside = (point) => {
      return norm(subtract(point, this.props.center)) < this.props.radius
    }
    const between = (x, v1, v2) => {
      return x > Math.min(v1, v2) && x < Math.max(v1, v2)
    }
    const intersectsSegment = (segment) => {
      const [start, end] = segment
      if (inside(start) || inside(end)) {
        return true
      }
      if (start.re === end.re) {
        if (!between(this.props.center.im, start.im, end.im)) {
          return false
        }
        return abs(this.props.center.re - start.re) < this.props.radius
      } else {
        if (!between(this.props.center.re, start.re, end.re)) {
          return false
        }
        return abs(this.props.center.im - start.im) < this.props.radius
      }
    }
    const x = tile.re, y = tile.im
    const edges = tileEdges(tile)
    for (let i = 0; i < 4; i++) {
      const segment = [edges[i], edges[(i + 1) % 4]]
      if (intersectsSegment(segment) || (between(this.props.center.re, x, x + 1) && between(this.props.center.im, y, y + 1))) {
        return true
      }
    }
    return false
  }
  get tiles() {
    const tiles = []
    const x = Math.floor(this.props.center.re), y = Math.floor(this.props.center.im)
    const offset = 2 * Math.ceil(this.props.radius)
    for (let i = x - offset; i < x + offset; i++) {
      for (let j = y - offset; j < y + offset; j++) {
        const tile = complex(i, j)
        if (this.intersects(tile)) {
          tiles.push(tile)
        }
      }
    }
    return tiles
  }
  render() {
    if (this.props.grid.transform === null) {
      return null
    }
    const points = []
    for (var i = 0; i < SPELL_RESOLUTION; i++) {
      const ang = (2 * Math.PI / SPELL_RESOLUTION) * i
      const point = Complex.fromPolar(this.props.radius, ang)
      const pixel = subtract(this.props.grid.toPixels(point), this.props.grid.origin)
      points.push(pixel.re)
      points.push(pixel.im)
    }
    const pixel = this.props.grid.toPixels(this.props.center)
    return (
      <>
        <Line
          x={pixel.re}
          y={pixel.im}
          points={points}
          stroke={this.props.color}
          strokeWidth={3}
          closed
        />
        <Circle
          x={pixel.re}
          y={pixel.im}
          radius={2}
          fill={this.props.color}
        />
        {
          this.props.renderTiles && this.tiles.map(tile => (
            <Tile edge={tile} grid={this.props.grid}/>
          ))
        }
      </>
    )
  }
}

export default Spell
