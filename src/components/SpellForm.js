import React, { useState } from 'react'
import styled from 'styled-components'
import { GithubPicker } from 'react-color'

import { PRESETS } from '../hooks/useSpell'
import SidebarSectionTitle from './SidebarSectionTitle'

const Container = styled.div`
  display: grid;
  row-gap: 10px;
  margin: 10px;
  justify-content: stretch;
`
const Select = styled.select`
  width: 180px;
  box-sizing: border-box;
`
const Field = styled.div`
  width: 180px;
  align-items: center;
  display: flex;
`
const Label = styled.div`
  display: inline-block;
  width: 50px;
  height: 25px;
  line-height: 25px;
  box-sizing: border-box;
  font-size: 16px;
`
const Input = styled.input`
  width: 130px;
  box-sizing: border-box;
`
const Color = styled.div`
  display: inline-block;
  height: 25px;
  width: 130px;
  background-color: ${props => props.color};
  border-radius: 5px;
`

const SpellForm = ({radius, color, setRadius, setColor, preset, setPreset}) => {
  const [showPicker, setShowPicker] = useState(false)

  return (
    <>
      <SidebarSectionTitle>
        Current Spell
      </SidebarSectionTitle>
      <Container>
        <Select
          onChange={(event) => {setPreset(event.target.value)}}
        >
          {
            Object.keys(PRESETS).map(key => (
              <option
                value={key}
                selected={key === preset}
              >
                {PRESETS[key].name}
              </option>
            ))
          }
        </Select>
        <Field>
          <Label>Radius</Label>
          <Input
            type="number" step="0.01" min="0.01" max="5"
            value={radius}
            onChange={(e) => setRadius(parseFloat(e.target.value))}
          />
        </Field>
        <Field>
          <Label>Color</Label>
          <Color
            color={color}
            onClick={() => {setShowPicker(!showPicker)}}
          />
        </Field>
        {
          showPicker && (
            <GithubPicker
              color={color}
              colors={['#006ffe', '#1000ff', '#8c4c0c', '#773b00']}
              width="100px"
              onChangeComplete={(color) => {setColor(color.hex)}}
            />
          )
        }
      </Container>
    </>
  )
}

export default SpellForm
