import React from 'react'

const CloseIcon = () => (
  <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M1 1L13 13M1 13L13 1" stroke="black" strokeWidth="2"/>
  </svg>
)

export default CloseIcon
