import React from 'react'
import { Line } from 'react-konva'
import { multiply, add } from 'mathjs'

import { AXIS1, AXIS2 } from '../hooks/useGrid'

const LINE_SIZE = 100000
const GRID_SIZE = 100

const Grid = ({origin, tileSize}) => {
  const gridLines = []
  for (let offset = 0; offset < GRID_SIZE; offset++) {
    let p = add(origin, multiply(AXIS2, tileSize * (offset - GRID_SIZE / 2)))
    let left = multiply(AXIS1, LINE_SIZE)
    let right = multiply(AXIS1, -LINE_SIZE)
    gridLines.push(
      <Line
        x={p.re}
        y={p.im}
        points={[left.re, left.im, right.re, right.im]}
        stroke="black"
        strokeWidth={1}
      />
    )
    p = add(origin, multiply(AXIS1, tileSize * (offset - GRID_SIZE / 2)))
    left = multiply(AXIS2, LINE_SIZE)
    right = multiply(AXIS2, -LINE_SIZE)
    gridLines.push(
      <Line
        x={p.re}
        y={p.im}
        points={[left.re, left.im, right.re, right.im]}
        stroke="black"
        strokeWidth={1}
      />
    )
  }
  return gridLines
}

export default Grid
