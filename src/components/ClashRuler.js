import React, { useState } from 'react'
import { Image, Layer, Stage } from 'react-konva'
import { complex, norm, subtract } from 'mathjs'
import styled from 'styled-components'

import Grid from './Grid'
import Spell from './Spell'
import SpellList from './SpellList'
import SpellForm from './SpellForm'

import useGrid, { STAGES } from '../hooks/useGrid'
import useSpell from '../hooks/useSpell'
import useSpellList from '../hooks/useSpellList'

const AppContainer = styled.div`
  display: grid;
  grid-template-areas: 'canvas sidebar';
  grid-template-columns: auto 200px;
  overflow: hidden;
  height: 100%;
`
const CanvasContainer = styled.div`
  grid-area: canvas;
  background-color: #c4c4c4;
  overflow: auto;
`
const SidebarContainer = styled.div`
  grid-area: sidebar;
  background-color: #fafafa;
  overflow: auto;
`

const TILE_SIZE_SENSIBILITY = 0.1

const ClashGrid = ({image}) => {
  const grid = useGrid(complex(image.width / 2, image.height / 2))
  const { spells, addSpell, removeSpell } = useSpellList()
  const [hoveringSpellKey, setHoveringSpellKey] = useState(null)
  const currentSpell = useSpell()
  const [showSpell, setShowSpell] = useState(true)

  const mouseMove = (event) => {
    const {layerX, layerY} = event.evt
    const location = complex(layerX, layerY)
    if (grid.stage === STAGES.ORIGIN) {
      grid.updateOrigin(location)
    } else if (grid.stage === STAGES.TILE_SIZE) {
      grid.updateTileSize(norm(subtract(location, grid.origin)) * TILE_SIZE_SENSIBILITY)
      setShowSpell(true)
    } else if (grid.stage === STAGES.DONE) {
      currentSpell.setCenter(grid.toPoint(location))
      setShowSpell(true)
    }
  }
  const mouseDown = () => {
    if (grid.stage === STAGES.ORIGIN) {
      grid.setOrigin(grid.origin)
    } else if (grid.stage === STAGES.TILE_SIZE) {
      grid.setTileSize(grid.tileSize)
    } else if (grid.stage === STAGES.DONE) {
      addSpell({
        center: currentSpell.center,
        radius: currentSpell.radius,
        color: currentSpell.color,
      })
    }
  }
  return (
    <AppContainer>
      <CanvasContainer>
        <Stage width={image.width} height={image.height}>
          <Layer
            onMouseDown={mouseDown}
            onMouseMove={mouseMove}
            onMouseLeave={() => {setShowSpell(false)}}
          >
            <Image
              image={image}
            />
            {
              grid.stage === STAGES.DONE || (
                <Grid
                  origin={grid.origin}
                  tileSize={grid.tileSize}
                />
              )
            }
            {
              spells.map(spell => (
                <Spell
                  center={spell.center}
                  radius={spell.radius}
                  color={spell.color}
                  grid={grid}
                  renderTiles={spell.key === hoveringSpellKey}
                />
              ))
            }
            {
              showSpell && (
                <Spell
                  center={currentSpell.center}
                  radius={currentSpell.radius}
                  color={currentSpell.color}
                  grid={grid}
                  renderTiles
                />
              )
            }
          </Layer>
        </Stage>
      </CanvasContainer>
      <SidebarContainer>
        <SpellForm
          radius={currentSpell.radius}
          setRadius={currentSpell.setRadius}
          color={currentSpell.color}
          setColor={currentSpell.setColor}
          preset={currentSpell.preset}
          setPreset={currentSpell.setPreset}
        />
        <SpellList
          spells={spells}
          onHoverStart={(key) => {setHoveringSpellKey(key)}}
          onHoverEnd={() => {setHoveringSpellKey(null)}}
          deleteSpell={(key) => {
            removeSpell(key)
            setHoveringSpellKey(null)
          }}
        />
      </SidebarContainer>
    </AppContainer>
  )
}

export default ClashGrid
