import styled from 'styled-components'

const SidebarSectionTitle = styled.div`
  background-color: #aaa;
  height: 30px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 20px;
  font-weight: bold;
`

export default SidebarSectionTitle
