build: FORCE
	docker build --tag reinodovo/raimunda . -f Dockerfile --platform linux/amd64

push: build
	docker push reinodovo/raimunda

deploy: FORCE
	-kubectl delete -f ./deploy
	kubectl apply -f ./deploy

FORCE: ;
